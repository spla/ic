#!/usr/bin/env python
# -*- coding: utf-8 -*-

from six.moves import urllib
from datetime import datetime
from subprocess import call
import time
import os
import json
import time
import signal
import sys
import os.path        # For checking whether secrets file exists
import requests       # For doing the web stuff, dummy!
import operator       # serveix per poder assignar el valor d'elements de un diccionari a una variable 15/07/18
import calendar
from importlib import reload

###############################################################################
# INITIALISATION
###############################################################################

# Returns the parameter from the specified file
def get_parameter( parameter, file_path ):
    # Check if secrets file exists
    if not os.path.isfile(file_path):    
        print("Archivo %s no encontrado, ejecuta 'python setup.py'."%file_path)
        sys.exit(0)

    # Find parameter in file
    with open( file_path ) as f:
        for line in f:
            if line.startswith( parameter ):
                return line.replace(parameter + ":", "").strip()

    # Cannot find parameter, exit
    print(file_path + "  Missing parameter %s "%parameter)
    sys.exit(0)

# Check if config.txt exists and count how many servers
config_filepath = "config.txt"
if os.path.isfile(config_filepath):
  with open(config_filepath) as f:
     instancias = sum(1 for _ in f)
else:
  sys.exit("Archivo %s no encontrado, ejecuta 'python setup.py'."%config_filepath)

###############################################################################
# get mrf info
###############################################################################

instancias_conocidas = []
politica_mrf = []
borrar_federation = []
marcar_media = []
borrar_media = []
rechazar = []

i = 0
while i < instancias:

  pleroma_hostname = get_parameter('instancia_'+str(i+1), config_filepath)
  res = requests.get('https://' + pleroma_hostname + '/nodeinfo/2.0.json?')

  mrf_policies = res.json()['metadata']['federation']['mrf_policies'] 
  fed_tl_removal = res.json()['metadata']['federation']['mrf_simple']['federated_timeline_removal']
  media_nsfw = res.json()['metadata']['federation']['mrf_simple']['media_nsfw']
  media_removal = res.json()['metadata']['federation']['mrf_simple']['media_removal']
  rejected = res.json()['metadata']['federation']['mrf_simple']['reject']

  instancias_conocidas.append(pleroma_hostname)
  politica_mrf.append(mrf_policies)
  borrar_federation.append(fed_tl_removal)
  marcar_media.append(media_nsfw)
  borrar_media.append(media_removal)
  rechazar.append(rejected)
  
  i += 1

i = 0
while i < instancias:

  print("-------------------------------")
  print("Nodo: " + instancias_conocidas[i])
  print("Policies: " + str(politica_mrf[i]))
  print("Federated timeline removal: " + str(borrar_federation[i]))
  print("Media nsfw: " + str(len(marcar_media[i])) + " " + str(marcar_media[i]))
  print("Media removal: " + str(len(borrar_media[i])) + " " + str(borrar_media[i]))
  print("Rejected: " + str(len(rechazar[i])) + " " + str(rechazar[i]))
  
  i += 1

print("\n")
print("Instancias rechazadas:")

i = 0
while i < instancias:

  print("-------------------------------")
  print("Nodo: " + instancias_conocidas[i])
  print("Rejected: " + str(len(rechazar[i])) + " " + str(rechazar[i]))

  i += 1

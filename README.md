# ic

Compartir la politica MRF con Instancias Pleroma Conocidas


### Instrucciones:

Clonar ic.py, crear y activar el entorno virtual de Python:

1. git clone https://gitlab.com/spla/ic.git 'tudirectorio'
2. virtualenv -p python3 'tudirectorio'
3. cd 'tudirectorio'
4. source bin/activate

Una vez seguidos los pasos de arriba ya estamos dentro del Python Virtual Environment:

1. Ejecutamos 'python setup.py' para configurar las instancias conocidas de confianza. Se crea el fichero config.txt.

2. Ejecutamos 'python ic.py' para ver la politica MRF de las instancias.

Nota: instalamos todos los paquetes necesarios con 'pip install -r requirements.txt'.

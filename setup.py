#!/usr/bin/env python
# -*- coding: utf-8 -*-

import getpass
import os
import sys

# Returns the parameter from the specified file
def get_parameter( parameter, file_path ):
    # Check if secrets file exists
    if not os.path.isfile(file_path):
        print("Archivo %s no encontrado."%file_path)
        write_parameter( parameter, file_path )

    # Find parameter in file
    with open( file_path ) as f:
        for line in f:
            if line.startswith( parameter ):
                return line.replace(parameter + ":", "").strip()

    # Cannot find parameter, exit
    print(file_path + "  No se encuentra el parametro %s "%parameter)
    sys.exit(0)

def write_parameter( parameter, file_path ):
  
  instancias = input("¿cuantas instancias quieres configurar? ")
  try:

    val = int(instancias)
    print("Configurando las instancias conocidas...")
    print("\n")
    i = 0
    while i < int(instancias):
      num_instancia = 'instancia_'+str(i+1)

      instancia = input("Instancia " + str(i+1) + ": ")
      with open(file_path, "a") as text_file:
        print(num_instancia+ ": {}".format(instancia), file=text_file)

      i += 1

  except ValueError:
    try:
      val = float(instancias)
      print("Eso es un número flotante. Número = ", val)
      sys.exit("Debes teclear un número de instancias.")
    except ValueError:
      print("No.. no es un número. Es una cadena.")
      sys.exit("Debes teclear un número de instancias.")

#############################################################################################
  
# Load configuration from config file
config_filepath = "config.txt"
instancia_1 = get_parameter("instancia_1", config_filepath) 

print("Hecho!")
print("Ahora puedes ejecutar 'python ic.py'!")
print("\n")
